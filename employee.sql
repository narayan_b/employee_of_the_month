DROP DATABASE IF EXISTS employee_of_the_month;
CREATE DATABASE IF NOT EXISTS employee_of_the_month;
USE employee_of_the_month;

CREATE TABLE department (
  department_id int PRIMARY KEY AUTO_INCREMENT,
  department_name varchar(255) UNIQUE
);

CREATE TABLE employee (
  employee_id int PRIMARY KEY AUTO_INCREMENT,
  employee_name varchar(255),
  department_id int,
  FOREIGN KEY (department_id) REFERENCES department (department_id)
);

CREATE TABLE work_period (
  work_period_id int PRIMARY KEY AUTO_INCREMENT,
  working_date date,
  shift_start_time int,
  shift_end_time int,
  break_time int,
  working_duration int,
  over_time int,
  location ENUM("Usine A", "Usine B", "Usine C", "Usine D"),
  employee_id int,
  FOREIGN KEY (employee_id) REFERENCES employee (employee_id)
);

GRANT ALL PRIVILEGES ON employee_of_the_month.* TO 'narayan23'@'localhost';
