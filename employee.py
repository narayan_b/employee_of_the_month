import pandas as pd
import mysql.connector
import datetime

xlsx = pd.ExcelFile("employee.xlsx")
employee_df = pd.read_excel(xlsx, "employees")

dfworkdays = pd.read_excel("employee.xlsx", sheet_name=None, skiprows=1)

del dfworkdays["employees"]

dates = []
employee_ids = []
shift_starts = []
shift_ends = []
break_durations = []
overtime_hours = []
locations = []

for sheet_name, df in dfworkdays.items():
    if len(df.columns) <= 6:
        date = sheet_name

        for index, row in df.iterrows():
            dates.append(date)
            employee_ids.append(row.get("id", None))
            shift_starts.append(row.get("shift_start_time", None))
            shift_ends.append(row.get("shift_end_time", None))
            break_durations.append(row.get("break_duration", None))
            overtime_hours.append(row.get("overtime_hours", None))
            locations.append(row.get("location", None))

data = {
    "Date": dates,
    "Employee ID": employee_ids,
    "Shift Start Time": shift_starts,
    "Shift End Time": shift_ends,
    "Break Duration": break_durations,
    "Overtime Hours": overtime_hours,
    "Location": locations
}

df_combined = pd.DataFrame(data)

connection = mysql.connector.connect(
    host="localhost",
    user="narayan23",
    password="coucou123",
    auth_plugin='mysql_native_password',
    database='employee_of_the_month'
)

cursor = connection.cursor()

for row in employee_df.iterrows():
    department_name = row[1].iloc[2]
    cursor.execute("SELECT department_id FROM department WHERE department_name = %s", (department_name,))
    already_exist = cursor.fetchone()
    if not already_exist:
        cursor.execute("""
            INSERT INTO department(department_name) VALUE(%s)
        """, (row[1].iloc[2],))
    
    cursor.execute("""
        INSERT INTO employee(employee_id, employee_name, department_id) 
        VALUE(%s, %s, (select department_id from department where department_name like %s))
    """, (row[1].iloc[0], row[1].iloc[1], row[1].iloc[2],))

for index, row in df_combined.iterrows():
    spl = row['Shift Start Time'].split(":")
    start_minute = int(spl[0]) * 60 + int(spl[1])
    
    end = row['Shift End Time'].split(":")
    end_minute = int(end[0]) * 60 + int(end[1])
    
    working_time = end_minute - start_minute - row["Break Duration"]

    ot = row['Overtime Hours']
    over_time = int(ot) * 60
    
    row = list(row)
    row[0] = datetime.datetime.strptime(row[0], "%d-%m-%Y")
    
    cursor.execute("""
        INSERT INTO work_period (working_date, employee_id, shift_start_time, shift_end_time, break_time, over_time, location, working_duration)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
    """, (row[0], row[1], start_minute, end_minute, row[4], over_time, row[6], working_time))

connection.commit()

cursor.close()
connection.close()